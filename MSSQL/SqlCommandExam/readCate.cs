using System;
using System.Data;
using System.Data.SqlClient;

namespace SqlCommandExam
{
    public partial class Program
    {
      public static void ReadCategory()
      {
          string sqlconnectStr     = "Data Source=localhost,1433;Initial Catalog=xtlab;User ID=SA;Password=Password123";
          SqlConnection connection = new SqlConnection(sqlconnectStr);
          connection.Open();

          using (SqlCommand command = new SqlCommand("SELECT DanhmucID, TenDanhMuc FROM Danhmuc;", connection))
          using (SqlDataReader reader = command.ExecuteReader())
          {
              if (reader.HasRows)
              {
                  // Đọc kết quả
                  while (reader.Read())
                  {
                      Console.WriteLine("{0}\t{1}", reader[0].ToString(), reader.GetString(1));
                  }
              }
              else  Console.WriteLine("No rows found.");
          }

          connection.Close();
      }
    }
}
