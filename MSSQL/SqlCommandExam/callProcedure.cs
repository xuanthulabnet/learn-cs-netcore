using System;
using System.Data;
using System.Data.SqlClient;

namespace SqlCommandExam
{
    public partial class Program
    {
      public static void CallStoredProcedure()
      {
            string sqlconnectStr     = "Data Source=localhost,1433;Initial Catalog=xtlab;User ID=SA;Password=Password123";
            SqlConnection connection = new SqlConnection(sqlconnectStr);
            connection.Open();

            SqlCommand cmd   = new SqlCommand("getproduct", connection);
            cmd.CommandType  = CommandType.StoredProcedure;
            cmd.Parameters.Add(
                new SqlParameter() {
                    ParameterName   = "@id",
                    SqlDbType       = SqlDbType.Int,
                    Value           = 10
                }
            );

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    var ten = reader["TenSanpham"];
                    var gia = reader["Gia"];

                    Console.WriteLine($"{ten} \t {gia}");
                }
            }


          connection.Close();
      }
    }
}
